const opt = {
    base:{
        demand: true,
        alias:'b'
    },
    limite:{
        alias:'l',
        default:10
    }
}

const argv = require('yargs')
    .command('listar', 'Imprime tabla',opt)
    .command('crear', 'Genera un archivo de la tabla',opt)
    .help()
    .argv;  

module.exports ={
    argv
}