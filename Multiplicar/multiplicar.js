//requires 
const fs = require('fs');

let ToListTable =(base, limite)=>{

    console.log('*****************'.green);
    console.log(`tabla de ${base}`.green);
    console.log('*****************'.rainbow);

    for(let i = 1; i <= limite; i++){ 
        console.log (`${base}*${i}=${base * i}`);
    }
}

let MakeTable = (base, limite) =>{

    return new Promise ((resolve, reject) =>{
        //aqui valida  si el valor es un numero
        if (!Number(base)){
            reject(`No se puede multiplicar sin numeros`)
            //si no es numero termina el proceso hasta aqui
            return;
        }
        
        let tableCont = ''

        for(let i = 1; i <= limite; i++){ 
            tableCont += (`${base}*${i}=${base * i}\n`);
        }

            const data = new Uint8Array(Buffer.from(tableCont));
        fs.writeFile(`B:/Node/bases-node/Table_m/Table${base}.txt`, tableCont,(err) => { 
            if (err) 
                reject (err)
                //console.log(`tabla${base}.txt`);  
            else 
            resolve (`tabla-${base}-al-${limite}.txt`)          
        });
    });   
}

module.exports = {
    MakeTable, ToListTable
}
